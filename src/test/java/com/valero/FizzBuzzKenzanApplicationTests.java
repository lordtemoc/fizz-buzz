package com.valero;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FizzBuzzKenzanApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void sampleCase() {
		final int number = 15;
		final String expectedResponse = "{\"Fizz\":[3,6,9,12],\"Buzz\":[5,10],\"FizzBuzz\":[15]}";

		String body = this.restTemplate.getForObject("/fizzbuzz/{number}", String.class, number);
		Assert.assertEquals(body, expectedResponse);
	}

	@Test
	public void nonIntegerParamResponse() {
		final String number = "hello";
		final String BAD_REQUEST_STATUS = "400";
		final String BAD_REQUEST_ERROR = "Bad Request";
		JSONObject response = null;
		String responseStatus = null;
		String responseError = null;

		try{
			String body = this.restTemplate.getForObject("/fizzbuzz/{number}", String.class, number);
			response = new JSONObject(body);

			responseStatus = response.getString("status");
			responseError = response.getString("error");

		} catch (JSONException jsonException){
			System.out.println("Error trying casting response body into JSONObject");
		}

		Assert.assertNotNull(response);
		Assert.assertNotNull(responseStatus);
		Assert.assertNotNull(responseError);
		Assert.assertEquals(BAD_REQUEST_STATUS, responseStatus);
		Assert.assertEquals(BAD_REQUEST_ERROR, responseError);
	}

}
