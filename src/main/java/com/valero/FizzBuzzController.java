package com.valero;

import org.springframework.web.bind.annotation.*;

/**
 * Created by valero on 3/1/18.
 */
@RestController
public class FizzBuzzController {

    @RequestMapping(value = "/fizzbuzz/{number}", method = RequestMethod.GET)
    public FizzBuzzFactory calculateFizzBuzz(@PathVariable("number") int number) {
        return new FizzBuzzFactory(number);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String calculateFizzBuzz() {
        return "Made with <3 by Cuauhtemoc Valero";
    }

}
