package com.valero;

import java.util.List;

public class Fizz extends Calculator{
    protected final int FIZZ_MULTIPLE = 3;

    public Fizz(int value) {
        super.multipleNumber = FIZZ_MULTIPLE;
        super.number = value;
    }

    @Override
    public List<Integer> getMultiples() {
        return super.getMultiples();
    }
}
