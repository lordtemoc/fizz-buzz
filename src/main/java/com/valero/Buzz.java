package com.valero;

import java.util.List;

/**
 * Created by valero on 3/1/18.
 */
public class Buzz extends Calculator{
    protected final int BUZZ_MULTIPLE = 5;

    public Buzz(int value) {
        super.multipleNumber = BUZZ_MULTIPLE;
        super.number = value;
    }

    @Override
    public List<Integer> getMultiples() {
        return super.getMultiples();
    }
}
