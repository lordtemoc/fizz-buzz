package com.valero;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by valero on 3/3/18.
 */
public class Calculator {
    protected int multipleNumber;
    protected int number;

    public List<Integer> getMultiples(){
        return IntStream.range(1, number).filter(x -> x % multipleNumber == 0).boxed().collect(Collectors.toList());
    }
}
