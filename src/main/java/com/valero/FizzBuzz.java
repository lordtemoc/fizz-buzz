package com.valero;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by valero on 3/1/18.
 */
public class FizzBuzz extends Calculator{
    protected final int FIZZ_MULTIPLE = 3;
    protected final int BUZZ_MULTIPLE = 5;

    public FizzBuzz(int value) {
        super.multipleNumber = value;
        super.number = value;
    }

    @Override
    public List<Integer> getMultiples(){
        return IntStream.rangeClosed(1, number).filter(x -> x % FIZZ_MULTIPLE == 0 && x % BUZZ_MULTIPLE == 0).boxed().collect(Collectors.toList());
    }
}
