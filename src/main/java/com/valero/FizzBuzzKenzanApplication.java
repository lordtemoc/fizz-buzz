package com.valero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
	In this application I use Factory method pattern. There is a father class call Calculator who has the properties and
	methods to calculate the multiples of a number. The subclasses (Fizz, Buzz, FizzBuzz) extends Calculator class to
	inherit and override the methods and calculating their result.
*/

@SpringBootApplication
public class FizzBuzzKenzanApplication {
	public static void main(String[] args) {
		SpringApplication.run(FizzBuzzKenzanApplication.class, args);
	}
}
