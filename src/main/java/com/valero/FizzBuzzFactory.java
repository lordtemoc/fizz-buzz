package com.valero;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Created by valero on 3/1/18.
 */
public class FizzBuzzFactory {
    @JsonProperty("Fizz")
    public List<Integer> fizz;

    @JsonProperty("Buzz")
    public List<Integer> buzz;

    @JsonProperty("FizzBuzz")
    public List<Integer> fizzbuzz;

    public FizzBuzzFactory(int value) {
        this.fizz = new Fizz(value).getMultiples();
        this.buzz = new Buzz(value).getMultiples();
        this.fizzbuzz = new FizzBuzz(value).getMultiples();
    }

}
